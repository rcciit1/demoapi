package com.restassured.crudOperations;

import org.apache.http.util.Asserts;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;

import java.io.File;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class updateEmployeeDetails {

	@Test
	public void testCase03(){
		
		RestAssured.baseURI="https://jsonplaceholder.typicode.com/";
		RestAssured.basePath="";
		File jsonFile= new File("C:\\Users\\dipan.chandra.dutta\\Rest_Test\\restAssuredFramework\\put.json");
		Response response = given().contentType(ContentType.JSON).log().all().body(jsonFile).put("/4");
		String responsebody = response.getBody().asString();
		Assert.assertEquals(responsebody.contains("SMITH"), true);
		response.prettyPrint();
	}
}
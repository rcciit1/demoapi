package com.restassured.crudOperations;

import org.apache.http.util.Asserts;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;

import java.io.File;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class deleteEmployeeDetails {

	@Test
	public void testCase04(){
		
		RestAssured.baseURI="https://jsonplaceholder.typicode.com/";
		RestAssured.basePath="";
		Response response = given().contentType(ContentType.JSON).log().all().delete("4");
		Response response1 = given().contentType(ContentType.JSON).log().all().get("users");
		String responsebody = response1.getBody().asString();
		Assert.assertEquals(responsebody.contains("XYZ"), false);
		response.prettyPrint();
	}
}

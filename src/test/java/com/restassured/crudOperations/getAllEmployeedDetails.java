package com.restassured.crudOperations;
import org.apache.http.util.Asserts;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class getAllEmployeedDetails {

	
	public void testCase01(){
		
		RestAssured.baseURI="https://jsonplaceholder.typicode.com";
		RestAssured.basePath="";
		Response response = given().contentType(ContentType.JSON).log().all().get("users");
		int code = response.getStatusCode();
		System.out.print("Status code is " + code);
		Assert.assertEquals(code, 200);
		response.prettyPrint();
	}
	
	
}

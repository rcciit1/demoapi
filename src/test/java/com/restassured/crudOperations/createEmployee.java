package com.restassured.crudOperations;

import org.apache.http.HttpRequest;
import org.apache.http.util.Asserts;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;

import static io.restassured.RestAssured.*;

import java.io.File;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class createEmployee {

	@Test(priority=0)
	public void testCase05(){
		
		RestAssured.baseURI="https://jsonplaceholder.typicode.com/";
		RestAssured.basePath="";
		RequestSpecification httprequest = RestAssured.given();
		JSONObject reqparam = new JSONObject();
		reqparam.put("name", "DIPAN");
		reqparam.put("salary", "20000");
		reqparam.put("age", "30");
		
		// Add a header stating that request body is a json
		httprequest.header("Content-Type","application/json");
		//Add the JSON to the body of the request
		httprequest.body(reqparam.toJSONString());
		
		//POST request
		Response response = httprequest.request(Method.POST,"/create");
		
		//capture response body to perform validations
		
		String responsebody = response.getBody().asString();
		Assert.assertEquals(responsebody.contains("DIPAN"), true);
		
		
	}
}
